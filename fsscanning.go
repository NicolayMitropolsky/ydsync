package main
import (
	"path/filepath"
	"io/ioutil"
	"sort"
)

func BuildEntriesTree(root string, subroot string) (entries []FileEntry, err error) {

	files, err := ioutil.ReadDir(root)
	if (err != nil) {
		return
	}

	for _, file := range files {
		nameOnServer := filepath.Join(subroot, file.Name())
		entry := FileEntry{ResType:"", Lastmodified:file.ModTime(), Href: nameOnServer, Displayname:file.Name()}
		if (file.IsDir()) {
			entry.ResType = "collection"
			inner, err := BuildEntriesTree(filepath.Join(root, file.Name()), nameOnServer)
			if (err != nil) {
				return nil, err
			}

			entry.SubEntries = inner
		}
		entries = append(entries, entry)

	}


	return
}


func Updates(from []FileEntry, to []FileEntry) (entries []FileEntry, err error) {

	var fi, ti int = 0, 0

	sort.Sort(NameSorter(from))
	sort.Sort(NameSorter(to))

	for fi < len(from) {

		if (ti >= len(to)) {
			entries = append(entries, from[fi])
			fi++
			continue
		}

		for (fi < len(from) && from[fi].Href < to[ti].Href) {
			entries = append(entries, from[fi])
			fi++
		}

		for (fi < len(from) && ti < len(to) && from[fi].Href > to[ti].Href) {
			ti++
		}


		if (ti < len(to) && fi < len(from) && from[fi].Href == to[ti].Href) {


			if (from[fi].ResType == to[ti].ResType && from[fi].isCollection()) {
				subupdates, err := Updates(from[fi].SubEntries, to[ti].SubEntries)
				if (err != nil) {
					return nil, err
				}
				entries = append(entries, subupdates...)
			} else if (from[fi].Lastmodified.After(to[ti].Lastmodified)) {
				entries = append(entries, from[fi])
			}

			fi++
			ti++
		}



	}
	return
}


