package main

import (
	"encoding/xml"
	"fmt"
	"os"
	"strings"
	"io"
	"time"
	"net/url"
)

type FileEntry struct {
	Href         string
	ResType      string
	Lastmodified time.Time
	Displayname  string
	Creationdate time.Time
	SubEntries []FileEntry
}

func (fe *FileEntry) isCollection() bool  {
	return fe.ResType == "collection"
}

type NameSorter []FileEntry

func (a NameSorter) Len() int           { return len(a) }
func (a NameSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NameSorter) Less(i, j int) bool { return a[i].Href < a[j].Href }

func printTree(fe FileEntry, indent string){
	fmt.Printf("%sentry %s\n", indent, fe.Href)

	for _, e := range fe.SubEntries {
		printTree(e, indent + "  ")
	}


}

func parseResponse(data io.Reader) (entries []FileEntry, err error){
	decoder := xml.NewDecoder(data)

	for {
		t, err := decoder.Token()
		if err != nil {
			if(err == io.EOF){
				break
			} else {
				fmt.Fprintf(os.Stderr, "error readinf %v\n", err)
				return entries, err
			}
		}
		switch tt := t.(type) {
		case xml.StartElement:
			if tt.Name.Local == "response" {
				fe, err := parseResponseEntry(decoder)
				if err != nil {
					fmt.Fprintf(os.Stderr, "error parseResponse %v\n", err)
					break
				}
				entries = append(entries, fe)
			}
		case xml.EndElement:
		}
	}

	return
}

func parseResponseEntry(decoder *xml.Decoder) (e FileEntry, err error) {
	var t xml.Token

	readprop := func(decoder *xml.Decoder) {

		readrestype:= func(decoder *xml.Decoder) {
			for {
				it, _ := decoder.Token()
				switch tt := it.(type) {
				case xml.CharData:
					text := strings.TrimSpace(string(tt))
					if text != "" {
						e.ResType = text
					}
				case xml.StartElement:
					e.ResType = strings.TrimSpace(string(tt.Name.Local))
				case xml.EndElement:
					if tt.Name.Local == "resourcetype" {
						return
					}

				default:
					fmt.Fprintf(os.Stderr, "error unknown token %T %v\n", tt, tt)
				}
			}
		}

		for {
			t, err = decoder.Token()
			if err != nil {
				fmt.Fprintf(os.Stderr, "error readinf %v", err)
				return
			}

			switch tt := t.(type) {
			case xml.StartElement:
				switch tt.Name.Local {
				case "resourcetype":
					readrestype(decoder)
				case "getlastmodified":
					it, _ := decoder.Token()
					ti,err := time.Parse(time.RFC1123, strings.TrimSpace(string(it.(xml.CharData))))
					if err != nil {
						return
					}
					e.Lastmodified = ti
				case "displayname":
					it, _ := decoder.Token()
					e.Displayname = strings.TrimSpace(string(it.(xml.CharData)))
				case "creationdate":
					it, _ := decoder.Token()
					ti,err := time.Parse(time.RFC3339, strings.TrimSpace(string(it.(xml.CharData))))
					if err != nil {
						return
					}
					e.Creationdate = ti

				}

			case xml.EndElement:
				if tt.Name.Local == "prop" {
					return
				}
			}
		}
	}

	readpropstat := func(decoder *xml.Decoder) {

		for {
			t, err = decoder.Token()
			if err != nil {
				fmt.Fprintf(os.Stderr, "error readinf %v", err)
				return
			}

			switch tt := t.(type) {
			case xml.StartElement:
				switch tt.Name.Local {
				case "prop":
					readprop(decoder)
				}

			case xml.EndElement:
				if tt.Name.Local == "propstat" {
					return
				}
			}
		}
	}

	for {
		t, err = decoder.Token()
		if err != nil {
			fmt.Fprintf(os.Stderr, "error readinf %v\n", err)
			return
		}

		switch tt := t.(type) {
		case xml.StartElement:
			switch tt.Name.Local {
			case "href":
				it, _ := decoder.Token()
				unesc, err := url.QueryUnescape(strings.TrimSpace(string(it.(xml.CharData))))
				if err != nil {
					fmt.Fprintf(os.Stderr, "error QueryUnescape %v\n", err)
					return e, err
				}
				unesc = strings.TrimSuffix(unesc, "/")
				e.Href = unesc
			case "propstat":
				readpropstat(decoder)
			}

		case xml.EndElement:
			if tt.Name.Local == "response" {
				return
			}
		}
	}
}
