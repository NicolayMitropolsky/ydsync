package main

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

type Client struct {
	Login    string
	Password string
}

func (cli *Client) Put(path FileEntry, file string) (err error) {

	var req *http.Request

	if !path.isCollection() {
		f, err := os.Open(file)
		if err != nil {
			return err
		}
		defer f.Close()
		req, err = http.NewRequest("PUT", "https://webdav.yandex.ru/"+path.Href, f)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error %v\n", err)
			return err
		}
		req.Header.Add("Accept", "*/*")
		req.Header.Add("Host", "webdav.yandex.ru")
		req.SetBasicAuth(cli.Login, cli.Password)
		md5, err := ComputeMd5(file)
		if err != nil {
			return err
		}
		req.Header.Add("Etag", hex.EncodeToString(md5))
		sha, err := ComputeSha256(file)
		if err != nil {
			return err
		}
		req.Header.Add("Sha256", hex.EncodeToString(sha))
		req.Header.Add("Expect", "100-continue")
		req.Header.Add("Content-Type", "application/binary")
		req.Header.Add("Content-Length", strconv.FormatInt(size(file), 10))
	} else {
		req, err = http.NewRequest("MKCOL", "https://webdav.yandex.ru/"+path.Href, nil)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error %v\n", err)
			return
		}
		req.Header.Add("Accept", "*/*")
		req.Header.Add("Host", "webdav.yandex.ru")
		req.SetBasicAuth(cli.Login, cli.Password)
	}

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		fmt.Fprintf(os.Stdout, "error in client %v\n", err)
		return
	}

	fmt.Printf("client responed %s\n", resp.Status)
	if resp.StatusCode != 201 {
		io.Copy(os.Stdout, resp.Body)
		fmt.Println()
	}
	return nil
}

func (client *Client) FindOn(path string) (entries []FileEntry, err error) {
	req, err := http.NewRequest("PROPFIND", "https://webdav.yandex.ru/"+path, nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error %v\n", err)
		return
	}

	req.Header.Add("Accept", "*/*")
	req.SetBasicAuth(client.Login, client.Password)
	req.Header.Add("Depth", "1")

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		fmt.Fprintf(os.Stdout, "error in client %v\n", err)
		return
	}
	//
	//	fmt.Fprintf(os.Stdout, "resp code :%v %v\n\n", resp.StatusCode, resp.Status)
	//	fmt.Fprintf(os.Stdout, "resp Header :%v\n\n", resp.Header)
	//io.Copy(os.Stdout, resp.Body)

	entries, err = parseResponse(resp.Body)

	resp.Body.Close()
	return
}

func (client *Client) LoadEagerlyOn(path string) (entries []FileEntry, err error) {

	loadedentries, err := client.FindOn(path)
	if err != nil {
		return
	}

	for i, e := range loadedentries {
		if i > 0 {
			if e.isCollection() {
				var sub []FileEntry
				sub, err = client.LoadEagerlyOn(e.Href)
				if err != nil {
					return
				}
				e.SubEntries = sub
				//fmt.Printf("e.SubEntries %v\n", e)
			}
			entries = append(entries, e)
		}
	}

	return
}

func inputClient() (client *Client) {
	var input string
	fmt.Print("Enter login: ")
	fmt.Scanln(&input)
	login := input
	fmt.Print("Enter password: ")
	fmt.Scanln(&input)
	passwd := input

	client = &Client{login, passwd}
	return
}

func Linearize(entries []FileEntry) (linearized []FileEntry) {
	for _, e := range entries {
		list := e.SubEntries
		e.SubEntries = []FileEntry{}
		linearized = append(linearized, e)
		if e.isCollection() {
			linearized = append(linearized, Linearize(list)...)
		}

	}
	return
}

func main() {

	flag.Parse()
	pathOnYd := flag.Arg(0)
	pathOnComputer := flag.Arg(1)

	if !flag.Parsed() || flag.NArg() < 2 {
		fmt.Fprintf(os.Stderr, "Usage of %s: [path-on-yandex-disk] [local-path]\n", os.Args[0])
		flag.PrintDefaults()
		return
	}

	client := inputClient()

	onServer, err := client.LoadEagerlyOn(pathOnYd)

	if err != nil {
		fmt.Fprintf(os.Stderr, "error %v\n", err)
		return
	}

//	fmt.Println()
//	fmt.Println("Tree on server:")
//	fmt.Println()
//
//	for _, e := range onServer {
//		printTree(e, "")
//	}

	local, err := BuildEntriesTree(pathOnComputer, pathOnYd)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		return
	}

//	fmt.Println("Tree on local:")
//	fmt.Println()
//
//	for _, e := range local {
//		printTree(e, "")
//	}

	updateList, err := Updates(local, onServer)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		return
	}

	updateList = Linearize(updateList)

	fmt.Println("Tree to update:")
	for _, e := range updateList {
		printTree(e, "")
	}

	for _, e := range updateList {
		p, err := filepath.Rel(pathOnYd, e.Href)
		fmt.Printf("sending %s\n", p)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			return
		}
		err = client.Put(e, filepath.Join(pathOnComputer, p))
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			return
		}
	}

}

func ComputeMd5(filePath string) ([]byte, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return result, err
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return result, err
	}

	return hash.Sum(result), nil
}

func ComputeSha256(filePath string) ([]byte, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return result, err
	}
	defer file.Close()

	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return result, err
	}

	return hash.Sum(result), nil
}

func size(filepath string) int64 {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	fi, err := f.Stat()
	if err != nil {
		log.Fatal(err)
	}
	return fi.Size()
}
